package io.highcreeksoftware.ktmailerlite

import io.highcreeksoftware.ktmailerlite.models.Subscriber
import io.highcreeksoftware.ktmailerlite.models.SubscriberRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

internal interface SubscriberService {
    @POST("subscriber")
    fun addSubscriber(@Body subscriber: SubscriberRequest): Call<Subscriber>
}