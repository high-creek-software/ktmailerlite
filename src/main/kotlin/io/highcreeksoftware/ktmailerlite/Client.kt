package io.highcreeksoftware.ktmailerlite

import io.highcreeksoftware.ktmailerlite.models.Subscriber
import io.highcreeksoftware.ktmailerlite.models.SubscriberRequest
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class Client(val prod: Boolean, val apiKey: String, val baseUrl: String = "https://api.mailerlite.com/api/v2/") {

    private val subscriberService: SubscriberService
    private val groupService: GroupService

    init {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor {chain ->
            val request = chain.request().newBuilder().addHeader("X-MailerLite-ApiKey", apiKey).build()
            chain.proceed(request)
        }
        if(!prod) {
            val loggingInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            httpClient.addInterceptor(loggingInterceptor)
        }
        val base = Retrofit.Builder().baseUrl(baseUrl).client(httpClient.build()).addConverterFactory(GsonConverterFactory.create()).build()

        subscriberService = base.create(SubscriberService::class.java)
        groupService = base.create(GroupService::class.java)
    }

    fun addSubscriber(email: String, name: String?, fields: Map<String, String>?): Subscriber? {
        val call = subscriberService.addSubscriber(SubscriberRequest(email, name, fields))
        val response = call.execute()
        if(!response.isSuccessful) {
            return null
        }

        return response.body()
    }

    fun addSubscriberToGroup(groupId: Int, email: String, name: String?, fields: Map<String, String>?): Subscriber? {
        val call = groupService.addSubscriberToGroup(groupId, SubscriberRequest(email, name, fields))
        val response = call.execute()
        if(!response.isSuccessful) {
            return null
        }

        return response.body()
    }
}