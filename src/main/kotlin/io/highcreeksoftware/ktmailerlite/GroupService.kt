package io.highcreeksoftware.ktmailerlite

import io.highcreeksoftware.ktmailerlite.models.Subscriber
import io.highcreeksoftware.ktmailerlite.models.SubscriberRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

internal interface GroupService {
    @POST("groups/{groupId}/subscribers")
    fun addSubscriberToGroup(@Path("groupId") groupId: Int, @Body subscriber: SubscriberRequest): Call<Subscriber>
}