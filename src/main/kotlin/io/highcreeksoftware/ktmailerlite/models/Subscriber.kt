package io.highcreeksoftware.ktmailerlite.models

import com.google.gson.annotations.SerializedName

data class Subscriber(val id: Int, val name: String, val email: String, val sent: Int, val opened: Int, val clicked: Int,
                      val type: String, val fields: List<Field>?, @SerializedName("date_subscribe") val dateSubscribe: String?,
                      @SerializedName("date_unsubscribe") val dateUnsubscribe: String?, @SerializedName("date_created") val dateCreated: String?)