package io.highcreeksoftware.ktmailerlite.models

data class Field(val key: String, val value: String, val type: String)