package io.highcreeksoftware.ktmailerlite.models

data class SubscriberRequest(val email: String?, val name: String?, val fields: Map<String, String>?)